import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests the correctness of the {@link State#events()} implementation.
 */
class StateEventsTest {

    @Test
    void startNextEvents(){
        State state = State.INITIAL;
        assertEquals(Arrays.asList(Event.START), state.events());
    }

    @Test
    void inProgressNextEvents() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START);
        assertEquals(
                Arrays.asList(Event.APPROVE, Event.CANCEL, Event.COMMENT, Event.DECLINE, Event.RECOMMEND),
                state.events());
    }

    @Test
    void ApprovedNextEvents() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START).transition(Event.APPROVE);
        assertEquals(Arrays.asList(Event.COMMENT, Event.RECOMMEND), state.events());
    }

    @Test
    void DeclinedNextEvents() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START).transition(Event.DECLINE);
        assertEquals(Arrays.asList(Event.COMMENT, Event.RECOMMEND), state.events());
    }

    @Test
    void CancelledNextEvents() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START).transition(Event.CANCEL);
        assertEquals(Arrays.asList(Event.COMMENT, Event.RECOMMEND), state.events());
    }



}