import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests the correctness of the state machine transitions implemented in the {@link State} class.
 */

class StateTransitionsTest {

    // INITIAL state transitions

    @Test
    void startWhenInitial() throws InvalidTransitionException {
        State state = State.INITIAL;
        assertEquals(State.IN_PROGRESS, state.transition(Event.START));
    }

    // IN PROGRESS state transitions

    @Test
    void recommendWhenInProgress() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START);
        assertEquals(State.IN_PROGRESS, state.transition(Event.RECOMMEND));
    }

    @Test
    void commentWhenInProgress() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START);
        assertEquals(State.IN_PROGRESS, state.transition(Event.COMMENT));
    }

    @Test
    void startWhenInProgress() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START);
        InvalidTransitionException exception = assertThrows(InvalidTransitionException.class, () -> state.transition(Event.START));
        assertEquals(State.IN_PROGRESS, exception.getState());
        assertEquals(Event.START, exception.getEvent());
    }

    @Test
    void approveWhenInProgress() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START);
        assertEquals(State.APPROVED, state.transition(Event.APPROVE));
    }

    @Test
    void cancelWhenInProgress() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START);
        assertEquals(State.CANCELLED, state.transition(Event.CANCEL));
    }

    @Test
    void declineWhenInProgress() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START);
        assertEquals(State.DECLINED, state.transition(Event.DECLINE));
    }

    // APPROVED state transitions

    @Test
    void recommendWhenApproved() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START).transition(Event.APPROVE);
        assertEquals(State.APPROVED, state.transition(Event.RECOMMEND));
    }

    @Test
    void commentWhenApproved() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START).transition(Event.APPROVE);
        assertEquals(State.APPROVED, state.transition(Event.COMMENT));
    }

    @Test
    void startWhenApproved() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START).transition(Event.APPROVE);
        InvalidTransitionException exception = assertThrows(InvalidTransitionException.class, () -> state.transition(Event.START));
        assertEquals(State.APPROVED, exception.getState());
        assertEquals(Event.START, exception.getEvent());
    }

    @Test
    void approveWhenApproved() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START).transition(Event.APPROVE);
        InvalidTransitionException exception = assertThrows(InvalidTransitionException.class, () -> state.transition(Event.APPROVE));
        assertEquals(State.APPROVED, exception.getState());
        assertEquals(Event.APPROVE, exception.getEvent());
    }

    @Test
    void cancelWhenApproved() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START).transition(Event.APPROVE);
        InvalidTransitionException exception = assertThrows(InvalidTransitionException.class, () -> state.transition(Event.CANCEL));
        assertEquals(State.APPROVED, exception.getState());
        assertEquals(Event.CANCEL, exception.getEvent());
    }

    @Test
    void declineWhenApproved() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START).transition(Event.APPROVE);
        InvalidTransitionException exception = assertThrows(InvalidTransitionException.class, () -> state.transition(Event.DECLINE));
        assertEquals(State.APPROVED, exception.getState());
        assertEquals(Event.DECLINE, exception.getEvent());
    }

    // CANCELLED state transitions

    @Test
    void recommendWhenCancelled() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START).transition(Event.CANCEL);
        assertEquals(State.CANCELLED, state.transition(Event.RECOMMEND));
    }

    @Test
    void commentWhenCancelled() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START).transition(Event.CANCEL);
        assertEquals(State.CANCELLED, state.transition(Event.COMMENT));
    }

    @Test
    void startWhenCancelled() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START).transition(Event.CANCEL);
        InvalidTransitionException exception = assertThrows(InvalidTransitionException.class, () -> state.transition(Event.START));
        assertEquals(State.CANCELLED, exception.getState());
        assertEquals(Event.START, exception.getEvent());
    }

    @Test
    void approveWhenCancelled() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START).transition(Event.CANCEL);
        InvalidTransitionException exception = assertThrows(InvalidTransitionException.class, () -> state.transition(Event.APPROVE));
        assertEquals(State.CANCELLED, exception.getState());
        assertEquals(Event.APPROVE, exception.getEvent());
    }

    @Test
    void cancelWhenCancelled() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START).transition(Event.CANCEL);
        InvalidTransitionException exception = assertThrows(InvalidTransitionException.class, () -> state.transition(Event.CANCEL));
        assertEquals(State.CANCELLED, exception.getState());
        assertEquals(Event.CANCEL, exception.getEvent());
    }

    @Test
    void declineWhenCancelled() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START).transition(Event.CANCEL);
        InvalidTransitionException exception = assertThrows(InvalidTransitionException.class, () -> state.transition(Event.DECLINE));
        assertEquals(State.CANCELLED, exception.getState());
        assertEquals(Event.DECLINE, exception.getEvent());
    }

    // DECLINED state transitions

    @Test
    void recommendWhenDeclined() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START).transition(Event.DECLINE);
        assertEquals(State.DECLINED, state.transition(Event.RECOMMEND));
    }

    @Test
    void commentWhenDeclined() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START).transition(Event.DECLINE);
        assertEquals(State.DECLINED, state.transition(Event.COMMENT));
    }

    @Test
    void startWhenDeclined() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START).transition(Event.DECLINE);
        InvalidTransitionException exception = assertThrows(InvalidTransitionException.class, () -> state.transition(Event.START));
        assertEquals(State.DECLINED, exception.getState());
        assertEquals(Event.START, exception.getEvent());
    }

    @Test
    void approveWhenDeclined() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START).transition(Event.DECLINE);
        InvalidTransitionException exception = assertThrows(InvalidTransitionException.class, () -> state.transition(Event.APPROVE));
        assertEquals(State.DECLINED, exception.getState());
        assertEquals(Event.APPROVE, exception.getEvent());
    }

    @Test
    void cancelWhenDeclined() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START).transition(Event.DECLINE);
        InvalidTransitionException exception = assertThrows(InvalidTransitionException.class, () -> state.transition(Event.CANCEL));
        assertEquals(State.DECLINED, exception.getState());
        assertEquals(Event.CANCEL, exception.getEvent());
    }

    @Test
    void declineWhenDeclined() throws InvalidTransitionException {
        State state = State.INITIAL.transition(Event.START).transition(Event.DECLINE);
        InvalidTransitionException exception = assertThrows(InvalidTransitionException.class, () -> state.transition(Event.DECLINE));
        assertEquals(State.DECLINED, exception.getState());
        assertEquals(Event.DECLINE, exception.getEvent());
    }


}