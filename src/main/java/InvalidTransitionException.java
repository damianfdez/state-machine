/**
 * Represents an invalid state machine transition.
 *
 * <p>The class {@code InvalidTransitionException} is a <em>checked exception</em> because it is a lower-level exception
 * that should be catch and if necessary re-thrown wrapped in a higher-level exception that is more appropriate for a
 * higher-level abstraction.
 */
public class InvalidTransitionException extends Exception {

    private State state;

    private Event event;

    public InvalidTransitionException(State state, Event event) {
        super(("Invalid transition from state " + state + " on event " + event));
        this.state = state;
        this.event = event;
    }

    // TODO  Use Lombok to generate getter methods

    public State getState() {
        return state;
    }

    public Event getEvent() {
        return event;
    }
}
