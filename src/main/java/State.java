import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Simple Enum-based state machine used to represent the state transitions of a {@link ValidBreach} or {@link Preapproval}
 *
 * <p> If the state machine becomes more complicated (e.g., needs to use sub-states) an implementation based on a richer
 * abstraction should be considered (e.g., an implementation based on Spring Statemachine).
 */

public enum State {

    INITIAL {
        @Override
        State transition(Event event) throws InvalidTransitionException {
            if (event == Event.START) {
                return IN_PROGRESS;
            } else {
                throw new InvalidTransitionException(INITIAL, event);
            }
        }

        @Override
        List<Event> events() {
            return Collections.unmodifiableList(Arrays.asList(Event.START));
        }
    },

    IN_PROGRESS {
        @Override
        State transition(Event event) throws InvalidTransitionException{

            if (event == Event.RECOMMEND || event == Event.COMMENT) {
                return IN_PROGRESS;
            } else if (event == Event.APPROVE) {
                return APPROVED;
            } else if (event == Event.DECLINE) {
                return DECLINED;
            } else if (event == Event.CANCEL) {
                return CANCELLED;
            } else {
                throw new InvalidTransitionException(IN_PROGRESS, event);
            }

        }

        @Override
        List<Event> events() {
            return Collections.unmodifiableList(
                    Arrays.asList(Event.APPROVE, Event.CANCEL, Event.COMMENT, Event.DECLINE, Event.RECOMMEND));
        }
    },

    APPROVED {
        @Override
        State transition(Event event) throws InvalidTransitionException{
            if (event == Event.RECOMMEND || event == Event.COMMENT) {
                return APPROVED;
            } else {
                throw new InvalidTransitionException(APPROVED, event);
            }
        }

        @Override
        List<Event> events() {
            return Collections.unmodifiableList(Arrays.asList(Event.COMMENT, Event.RECOMMEND));
        }
    },

    DECLINED {
        @Override
        State transition(Event event) throws InvalidTransitionException {
            if (event == Event.RECOMMEND || event == Event.COMMENT) {
                return DECLINED;
            } else {
                throw new InvalidTransitionException(DECLINED, event);
            }
        }

        @Override
        List<Event> events() {
            return Collections.unmodifiableList(Arrays.asList(Event.COMMENT, Event.RECOMMEND));
        }
    },

    CANCELLED {
        @Override
        State transition(Event event) throws InvalidTransitionException {
            if (event == Event.RECOMMEND || event == Event.COMMENT) {
                return CANCELLED;
            } else {
                throw new InvalidTransitionException(CANCELLED, event);
            }
        }

        @Override
        List<Event> events() {
            return Collections.unmodifiableList(Arrays.asList(Event.COMMENT, Event.RECOMMEND));
        }
    };

    State transition(Event event) throws InvalidTransitionException{
        throw new InvalidTransitionException(null, event);
    }

    List<Event> events() {
        return Collections.emptyList();

    }

}
