/**
 * Represents the lifecycle events of a {@link ValidBreach} and {@link Preapproval}.
 *
 * <p>
 * The {@link State} class accepts these events and triggers state machine transitions accordingly.
 * </p>
 *
 */

public enum Event {
    START, COMMENT, RECOMMEND, APPROVE, DECLINE, CANCEL
}
